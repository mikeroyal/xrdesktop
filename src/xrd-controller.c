/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "xrd-controller.h"
#include "xrd-math.h"
#include "graphene-ext.h"

struct _XrdController
{
  GObject parent;

  guint64 controller_handle;
  XrdPointer *pointer_ray;
  XrdPointerTip *pointer_tip;
  XrdHoverState hover_state;
  XrdGrabState grab_state;

  graphene_matrix_t pose_hand_grip;
  graphene_matrix_t pose_pointer;

  graphene_matrix_t intersection_pose;
  gboolean ignore_input;
  GxrContext *context;
};

G_DEFINE_TYPE (XrdController, xrd_controller, G_TYPE_OBJECT)

static void
xrd_controller_finalize (GObject *gobject);

static void
xrd_controller_class_init (XrdControllerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = xrd_controller_finalize;
}

static void
xrd_controller_init (XrdController *self)
{
  self->hover_state.distance = 1.0f;
  self->hover_state.window = NULL;
  self->grab_state.window = NULL;
  self->grab_state.transform_lock = XRD_TRANSFORM_LOCK_NONE;
  graphene_matrix_init_identity (&self->pose_pointer);
  graphene_matrix_init_identity (&self->pose_hand_grip);
  self->pointer_ray = NULL;
  self->pointer_tip = NULL;
  self->ignore_input = FALSE;
  self->context = NULL;
}

XrdController *
xrd_controller_new (guint64     controller_handle,
                    GxrContext *context)
{
  XrdController *controller =
    (XrdController*) g_object_new (XRD_TYPE_CONTROLLER, 0);
  controller->controller_handle = controller_handle;
  controller->context = context;
  return controller;
}

static void
xrd_controller_finalize (GObject *gobject)
{
  XrdController *self = XRD_CONTROLLER (gobject);
  if (self->pointer_ray)
    g_object_unref (self->pointer_ray);
  if (self->pointer_tip)
    g_object_unref (self->pointer_tip);
}

XrdPointer *
xrd_controller_get_pointer (XrdController *self)
{
  return self->pointer_ray;
}

XrdPointerTip *
xrd_controller_get_pointer_tip (XrdController *self)
{
  return self->pointer_tip;
}

void
xrd_controller_set_pointer (XrdController *self, XrdPointer *pointer)
{
  self->pointer_ray = pointer;
}

void
xrd_controller_set_pointer_tip (XrdController *self, XrdPointerTip *tip)
{
  self->pointer_tip = tip;
}

guint64
xrd_controller_get_handle (XrdController *self)
{
  return self->controller_handle;
}

XrdHoverState *
xrd_controller_get_hover_state (XrdController *self)
{
  return &self->hover_state;
}

XrdGrabState *
xrd_controller_get_grab_state (XrdController *self)
{
  return &self->grab_state;
}

void
xrd_controller_reset_grab_state (XrdController *self)
{
  self->grab_state.window = NULL;
  graphene_point3d_init (&self->grab_state.grab_offset, 0, 0, 0);
  graphene_quaternion_init_identity (
    &self->grab_state.inverse_controller_rotation);
  graphene_quaternion_init_identity (&self->grab_state.window_rotation);
  self->grab_state.transform_lock = XRD_TRANSFORM_LOCK_NONE;
}

void
xrd_controller_reset_hover_state (XrdController *self)
{
  self->hover_state.window = NULL;
  graphene_point_init (&self->hover_state.intersection_2d, 0, 0);
  self->hover_state.distance = 1.0;
}

void
xrd_controller_update_hand_grip_pose (XrdController *self,
                                      graphene_matrix_t *pose)
{
  graphene_matrix_init_from_matrix (&self->pose_hand_grip, pose);
}

void
xrd_controller_get_hand_grip_pose (XrdController *self,
                                   graphene_matrix_t *pose)
{
  graphene_matrix_init_from_matrix (pose, &self->pose_hand_grip);
}

void
xrd_controller_update_pointer_pose (XrdController *self,
                                    graphene_matrix_t *pose)
{
  graphene_matrix_init_from_matrix (&self->pose_pointer, pose);
  xrd_pointer_move (self->pointer_ray, pose);
}

void
xrd_controller_update_hovered_window (XrdController *self,
                                      XrdWindow *last_window,
                                      XrdWindow *window,
                                      graphene_point3d_t *intersection_point,
                                      float intersection_distance)
{
  if (last_window != window)
    xrd_pointer_tip_set_active (self->pointer_tip, window != NULL);

  if (window)
    {
      self->hover_state.window = window;
      xrd_window_get_intersection_2d (
        window, intersection_point, &self->hover_state.intersection_2d);
      self->hover_state.distance = intersection_distance;

      graphene_matrix_t window_pose;
      xrd_window_get_transformation_no_scale (window, &window_pose);
      xrd_pointer_tip_update (self->pointer_tip, self->context,
                              &window_pose, intersection_point);

      xrd_pointer_set_length (self->pointer_ray, intersection_distance);

      if (self->ignore_input && !last_window)
        xrd_controller_show_pointer (self);
    }
  else
    {
      graphene_quaternion_t controller_rotation;
      graphene_quaternion_init_from_matrix (&controller_rotation,
                                            &self->pose_pointer);

      graphene_point3d_t distance_point;
      graphene_point3d_init (&distance_point,
                             0.f,
                             0.f,
                             -xrd_pointer_get_default_length (self->pointer_ray));

      graphene_point3d_t controller_position;
      graphene_ext_matrix_get_translation_point3d (&self->pose_pointer,
                                                   &controller_position);

      graphene_matrix_t tip_pose;
      graphene_matrix_init_identity (&tip_pose);
      graphene_matrix_translate (&tip_pose, &distance_point);
      graphene_matrix_rotate_quaternion (&tip_pose, &controller_rotation);
      graphene_matrix_translate (&tip_pose, &controller_position);
      xrd_pointer_tip_set_transformation (self->pointer_tip, &tip_pose);
      xrd_pointer_tip_update_apparent_size (self->pointer_tip, self->context);


      if (last_window)
        {
          xrd_pointer_reset_length (self->pointer_ray);
          xrd_controller_reset_hover_state (self);
          xrd_controller_reset_grab_state (self);
        }

      if (self->ignore_input && last_window)
        xrd_controller_hide_pointer (self);
    }
  xrd_pointer_set_selected_window (self->pointer_ray, window);

}

void
xrd_controller_hide_pointer (XrdController *self)
{
  xrd_pointer_hide (self->pointer_ray);
  xrd_pointer_tip_hide (self->pointer_tip);
}

void
xrd_controller_show_pointer (XrdController *self)
{
  xrd_pointer_show (self->pointer_ray);
  xrd_pointer_tip_show (self->pointer_tip);
}

gboolean
xrd_controller_is_pointer_visible (XrdController *self)
{
  return
    xrd_pointer_is_visible (self->pointer_ray) &&
    xrd_pointer_tip_is_visible (self->pointer_tip);
}

void
xrd_controller_drag_start (XrdController *self, XrdWindow *window)
{
  self->grab_state.window = window;

  graphene_quaternion_t controller_rotation;
  graphene_ext_matrix_get_rotation_quaternion (&self->pose_pointer,
                                               &controller_rotation);

  graphene_matrix_t window_transform;
  xrd_window_get_transformation_no_scale (self->grab_state.window,
                                          &window_transform);

  graphene_ext_matrix_get_rotation_quaternion (&window_transform,
                                               &self->grab_state.window_rotation);

  graphene_point3d_t distance_translation_point;
  graphene_point3d_init (&distance_translation_point,
                         0.f, 0.f, -self->hover_state.distance);

  graphene_point3d_init (
    &self->grab_state.grab_offset,
    -self->hover_state.intersection_2d.x,
    -self->hover_state.intersection_2d.y,
    0.f);

  graphene_quaternion_invert (
    &controller_rotation,
    &self->grab_state.inverse_controller_rotation);
}

gboolean
xrd_controller_intersects (XrdController *self,
                           XrdWindow *window,
                           graphene_point3d_t *intersection)
{
  float distance;
  graphene_vec3_t intersection_vec;
  bool intersects = xrd_pointer_get_intersection (self->pointer_ray, window,
                                                  &distance, &intersection_vec);

  graphene_point3d_init_from_vec3 (intersection, &intersection_vec);

  return intersects;
}

float
xrd_controller_get_distance (XrdController *self, graphene_point3d_t *point)
{
  return xrd_math_point_matrix_distance (point, &self->pose_pointer);
}

void
xrd_controller_get_pointer_pose (XrdController     *self,
                                 graphene_matrix_t *pose)
{
  graphene_matrix_init_from_matrix (pose, &self->pose_pointer);
}

void
xrd_controller_drag_window (XrdController *self,
                            XrdWindow     *window)
{
  graphene_point3d_t controller_translation_point;
  graphene_ext_matrix_get_translation_point3d (&self->pose_pointer,
                                               &controller_translation_point);
  graphene_quaternion_t controller_rotation;
  graphene_quaternion_init_from_matrix (&controller_rotation,
                                        &self->pose_pointer);

  graphene_point3d_t distance_translation_point;
  graphene_point3d_init (&distance_translation_point,
                         0.f, 0.f, -self->hover_state.distance);

  /* Build a new transform for pointer tip in event->pose.
   * Pointer tip is at intersection, in the plane of the window,
   * so we can reuse the tip rotation for the window rotation. */
  XrdGrabEvent *event = g_malloc (sizeof (XrdGrabEvent));
  event->controller_handle = self->controller_handle;
  graphene_matrix_init_identity (&self->intersection_pose);

  /* restore original rotation of the tip */
  graphene_matrix_rotate_quaternion (&self->intersection_pose,
                                     &self->grab_state.window_rotation);

  /* Later the current controller rotation is applied to the overlay, so to
   * keep the later controller rotations relative to the initial controller
   * rotation, rotate the window in the opposite direction of the initial
   * controller rotation.
   * This will initially result in the same window rotation so the window does
   * not change its rotation when being grabbed, and changing the controllers
   * position later will rotate the window with the "diff" of the controller
   * rotation to the initial controller rotation. */
  graphene_matrix_rotate_quaternion (
    &self->intersection_pose, &self->grab_state.inverse_controller_rotation);

  /* then translate the overlay to the controller ray distance */
  graphene_matrix_translate (&self->intersection_pose,
                             &distance_translation_point);

  /* Rotate the translated overlay to where the controller is pointing. */
  graphene_matrix_rotate_quaternion (&self->intersection_pose,
                                     &controller_rotation);

  /* Calculation was done for controller in (0,0,0), just move it with
   * controller's offset to real (0,0,0) */
  graphene_matrix_translate (&self->intersection_pose,
                             &controller_translation_point);



  graphene_matrix_t transformation_matrix;
  graphene_matrix_init_identity (&transformation_matrix);

  /* translate such that the grab point is pivot point. */
  graphene_matrix_translate (&transformation_matrix,
                             &self->grab_state.grab_offset);

  /* window has the same rotation as the tip we calculated in event->pose */
  graphene_matrix_multiply (&transformation_matrix,
                            &self->intersection_pose,
                            &transformation_matrix);

  xrd_window_set_transformation (window,
                                 &transformation_matrix);

  XrdPointer *pointer = self->pointer_ray;
  xrd_pointer_set_selected_window (pointer, window);


  xrd_pointer_tip_set_transformation (self->pointer_tip,
                                      &self->intersection_pose);
  /* update apparent size after pointer has been moved */
  xrd_pointer_tip_update_apparent_size (self->pointer_tip, self->context);

  xrd_window_emit_grab (window, event);
}

void
xrd_controller_set_ignore_input (XrdController *self,
                                 gboolean ignore_input)
{
  self->ignore_input = ignore_input;
  if (!ignore_input)
    {
      xrd_controller_show_pointer (self);
      return;
    }

  if (self->hover_state.window)
    xrd_controller_show_pointer (self);
  else if (!self->hover_state.window && self->ignore_input)
    xrd_controller_hide_pointer (self);
}
